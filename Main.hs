module Main (main, FileId) where

import           Control.Exception (catchJust)
import           Control.Lens (Lens', lens, (%~))
import           Control.Monad (guard, replicateM_)
import           Control.Monad.IO.Class (MonadIO, liftIO)
import           Control.Monad.Logger (LogLevel (LevelDebug), LoggingT,
                                       MonadLogger, filterLogger, logDebugS,
                                       runStderrLoggingT)
import           Control.Monad.Reader (ReaderT)
import           Control.Monad.Trans.Resource (ResourceT, runResourceT)
import qualified Data.ByteString as BS
import qualified Data.ByteString.Char8 as BSC
import           Data.Fixed (Pico)
import           Data.Foldable (for_)
import           Data.Function ((&))
import           Data.Int (Int64)
import qualified Data.Map.Strict as Map
import           Data.Maybe (isJust)
import           Data.Text (Text)
import qualified Data.Text as Text
import           Data.Time (TimeOfDay, UTCTime, getCurrentTime)
import qualified Data.Time as Time
import           Data.Traversable (for)
import           Data.Word (Word64)
import           Database.Persist (Entity (..), Filter,
                                   SelectOpt (Asc, Desc, LimitTo), count,
                                   delete, getEntity, getJust, insertKey,
                                   selectFirst, selectKeysList, selectList,
                                   toPersistValue, update, (=.), (==.))
import           Database.Persist.Sql (SqlBackend, rawExecute, runMigration,
                                       runSqlConn)
import           Database.Persist.Sqlite (withSqliteConn)
import           Database.Persist.TH (mkMigrate, mkPersist, persistLowerCase,
                                      share, sqlSettings)
import           GHC.Stack (HasCallStack, callStack)
import           System.IO.Error (isDoesNotExistError, isPermissionError)
import           System.Posix.ByteString (DeviceID, RawFilePath, deviceID,
                                          getSymbolicLinkStatus, isDirectory)
import qualified System.Posix.ByteString as Posix
import           System.Posix.Directory.Traversals (getDirectoryContents)
import           System.Posix.FilePath ((</>))
import           System.TimeIt (timeItT)

share
  [mkPersist sqlSettings, mkMigrate "migrateAll"]
  [persistLowerCase|
    File
      Id          Word64
      isDirectory Bool
      parent      FileId Maybe
      path        RawFilePath
      size        Int64
      sumSize     Int64
      updated     UTCTime
      -- volatile    Bool

      deriving Show
  |]

data Options = Options
  { updateBatchSize :: Int
  , fatFilesLimit   :: Int
  }

main :: IO ()
main = do
  let options@Options{..} = Options{updateBatchSize = 1000, fatFilesLimit = 20}
        -- ^ TODO parse command line
  Just root <- getFileStatus Nothing "/"
  initDb
  (updateTime, ()) <- timeItT $ updateOldestFiles root updateBatchSize
  report options updateTime

updateOldestFiles :: (DeviceID, Entity File) -> Int -> IO ()
updateOldestFiles (rootDeviceId, Entity rootInode root) updateBatchSize =
  replicateM_ updateBatchSize do
    mOldestFile <- runDb $ selectFirst [] [Asc FileUpdated]
    case mOldestFile of
      Nothing         -> runDb $ insertKey rootInode root
      Just oldestFile -> updateFile rootDeviceId oldestFile

getFileStatus ::
  Maybe FileId -> RawFilePath -> IO (Maybe (DeviceID, Entity File))
getFileStatus fileParent filePath = do
  fileUpdated <- getCurrentTime
  catchJust
    (\e -> guard $ isDoesNotExistError e || isPermissionError e)
    do
      status <- getSymbolicLinkStatus filePath
      let Posix.CIno fileInode = Posix.fileID   status
          Posix.COff fileSize  = Posix.fileSize status
      pure $
        Just
          ( deviceID status
          , Entity
              (FileKey fileInode)
              File
                { fileIsDirectory = isDirectory status
                , fileParent
                , filePath
                , fileSize
                , fileSumSize = fileSize
                , fileUpdated
                }
          )
    (\() -> pure Nothing)

updateFile :: DeviceID -> Entity File -> IO ()
updateFile rootDeviceId entity@(Entity fileId file) =
  do
    putStr $ show (previousUpdate & seconds %~ fromIntegral @Int . round) <> " "
    BSC.putStrLn filePath
    mStatus <- getFileStatus Nothing filePath
    case mStatus of
      Nothing                       -> runDb $ deleteTree fileId
      Just (deviceId', Entity id' file')
        | deviceId' /= rootDeviceId -> runDb $ deleteTree fileId
        | isDirectory'              ->
            updateDirectory rootDeviceId entity (id', size')
        | not isDirectory'          -> updateSimpleFile entity (id', size')
        | otherwise                 -> undefined
        where
          File{fileIsDirectory = isDirectory', fileSize = size'} = file'
  where
    File{filePath, fileUpdated = previousUpdate} = file

deleteTree ::
  (HasCallStack, MonadIO m, MonadLogger m) => FileId -> ReaderT SqlBackend m ()
deleteTree dirId = do
  $logDebugS "deleteTree" $ "dirId = " <> tshow dirId
  $logDebugS "deleteTree" $ "reason = " <> tshow callStack
  children <- selectKeysList [FileParent ==. Just dirId] []
  for_ children deleteTree
  delete dirId

updateDirectory :: DeviceID -> Entity File -> (FileId, Int64) -> IO ()
updateDirectory rootDeviceId (Entity fileId file) (fileId', size') =
  do
    now <- getCurrentTime
    updateDirectoryListing rootDeviceId fileId filePath
    runDb if
      | fileId == fileId', fileSize == size' ->
          update fileId [FileUpdated =. now]
      | fileId == fileId' -> do
          update fileId [FileSize =. size', FileUpdated =. now]
          updateSumSize fileId
      | otherwise -> do
          deleteTree fileId
          insertDir fileId' fileParent filePath size'
          updateSumSize fileId'
  where
    File{fileSize, filePath, fileParent} = file

updateSimpleFile :: Entity File -> (FileId, Int64) -> IO ()
updateSimpleFile (Entity fileId file) (fileId', size') =
  do
    now <- getCurrentTime
    runDb if
      | fileId == fileId', fileSize == size' ->
          update fileId [FileUpdated =. now]
      | fileId == fileId' -> do
          update
            fileId
            [FileSize =. size', FileSumSize =. size', FileUpdated =. now]
          for_ fileParent updateSumSize
      | otherwise -> do
          deleteTree fileId
          insertRegularFile fileId' fileParent filePath size'
          for_ fileParent updateSumSize
  where
    File{fileParent, filePath, fileSize} = file

insertFile ::
  (MonadIO m, MonadLogger m) =>
  Bool ->
  FileId ->
  Maybe FileId ->
  RawFilePath ->
  Int64 ->
  ReaderT SqlBackend m ()
insertFile fileIsDirectory fileId fileParent filePath fileSize = do
  now <- liftIO getCurrentTime
  insertCheckingInode $
    Entity
      fileId
      File
        { fileIsDirectory
        , fileParent
        , filePath
        , fileSize
        , fileSumSize = fileSize
        , fileUpdated = now
        }

insertDir ::
  (MonadIO m, MonadLogger m) =>
  FileId -> Maybe FileId -> RawFilePath -> Int64 -> ReaderT SqlBackend m ()
insertDir = insertFile True

insertRegularFile ::
  (MonadIO m, MonadLogger m) =>
  FileId -> Maybe FileId -> RawFilePath -> Int64 -> ReaderT SqlBackend m ()
insertRegularFile = insertFile False

updateSumSize :: MonadIO m => FileId -> ReaderT SqlBackend m ()
updateSumSize dirId = do
  rawExecute
    "update File \
    \set sum_size = \
      \size + (select ifnull(sum(sum_size), 0) from File where parent = ?) \
    \where id = ?"
    (replicate 2 $ toPersistValue dirId)
  File{fileParent} <- getJust dirId
  for_ fileParent updateSumSize

updateDirectoryListing :: DeviceID -> FileId -> RawFilePath -> IO ()
updateDirectoryListing rootDeviceId dirId dir = do
  filesActualContents <-
    catchJust
      (guard . isPermissionError)
      (getDirectoryContents dir)
      (\() -> pure [])
  let
    filesActualPaths =
      [ name
      | (_dirType, name) <- filesActualContents, name /= ".", name /= ".."
      ]
  filesActualRecords <-
    for filesActualPaths \name -> let
      path = dir </> name
      in getFileStatus (Just dirId) path
  let
    filesActual =
      Map.fromList
        [ (filePath, file)
        | Just (deviceId, file@(Entity _ File{filePath})) <- filesActualRecords
        , deviceId == rootDeviceId
        ]
  runDb do
    $logDebugS "updateDirectoryListing" $
      "filesActual.keys = " <> tshow (Map.keys filesActual)
    filesCached' <- selectList [FileParent ==. Just dirId] []
    let
      filesCached =
        Map.fromList
          [(filePath, fileId) | Entity fileId File{filePath} <- filesCached']
    $logDebugS "updateDirectoryListing" $
      "filesCached.keys = " <> tshow (Map.keys filesCached)
    let filesCachedToDelete = filesCached `Map.difference` filesActual
    $logDebugS "updateDirectoryListing" $
      "filesCachedToDelete.keys = " <> tshow (Map.keys filesCachedToDelete)
    for_ (Map.assocs filesCachedToDelete) \(f, i) -> do
      $logDebugS "updateDirectoryListing.delete" $ "f = " <> tshow f
      deleteTree i
    let filesActualToInsert = filesActual `Map.difference` filesCached
    for_ filesActualToInsert insertCheckingInode

insertCheckingInode ::
  (MonadIO m, MonadLogger m) => Entity File -> ReaderT SqlBackend m ()
insertCheckingInode (Entity fileId file@File{filePath}) = do
  mExistingFile <- getEntity fileId
  case mExistingFile of
    Nothing -> insertKey fileId file
    Just (Entity existingFileId File{filePath = existingFilePath})
      | BS.length existingFilePath <= BS.length filePath -> pure ()
      | otherwise -> do
          deleteTree existingFileId
          insertKey fileId file

report :: Options -> Double -> IO ()
report Options{fatFilesLimit, updateBatchSize} updateTime = do
  putStrLn ""
  putStrLn $
    "average update speed: " <>
    show (fromIntegral updateBatchSize / updateTime) <> " files/s, " <>
    show (updateTime / fromIntegral updateBatchSize) <> " s/file"
  totalCount <- runDb $ count ([] :: [Filter File])
  putStrLn $ "total: " <> show totalCount
  fatFiles <-
    runDb $
    map entityVal <$> selectList [] [Desc FileSumSize, LimitTo fatFilesLimit]
  putStrLn "fat:"
  for_
    fatFiles
    \File{fileSumSize, filePath, fileIsDirectory, fileParent, fileUpdated} ->
      BSC.putStrLn $
        "\t" <> BSC.pack (showHuman fileSumSize) <>
        "\t" <>
          filePath <>
          (if fileIsDirectory && isJust fileParent then "/" else "") <>
        "\t" <> BSC.pack (show fileUpdated)

showHuman :: Int64 -> String
showHuman n
  | n < k     = show n <> " B"
  | n < m     = show (n `div` k) <> " kB"
  | n < g     = show (n `div` m) <> " MB"
  | otherwise = show (n `div` g) <> " GB"
  where
    k = 1_000
    m = 1_000_000
    g = 1_000_000_000

runDb :: ReaderT SqlBackend (LoggingT (ResourceT IO)) a -> IO a
runDb action =
  runResourceT $
  runStderrLoggingT $
  filterLogger (\_source level -> level > LevelDebug) $
  withSqliteConn "files.sqlite" $
  runSqlConn action

initDb :: IO ()
initDb =
  runDb do
    runMigration migrateAll
    rawExecute "create index if not exists file_parent  on file (parent )" []
    rawExecute "create index if not exists file_updated on file (updated)" []

time :: Lens' UTCTime TimeOfDay
time =
  lens
    (Time.timeToTimeOfDay . Time.utctDayTime)
    (\t new -> t{Time.utctDayTime = Time.timeOfDayToTime new})

seconds :: Lens' UTCTime Pico
seconds = time <$> lens Time.todSec (\t new -> t{Time.todSec = new})

tshow :: Show a => a -> Text
tshow = Text.pack . show
